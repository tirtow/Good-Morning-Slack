# Good Morning Slack
**Good-Morning-Slack** is a Python script that scrapes information such as
weather and current events from various web sources, formats the information,
and sends it as message to the given Slack channel.

![Example Image](img/full.jpeg?raw=true)

## Usage
```
$ python3 good_morning_slack.py
```

## Setup
1. Install requirements:
```
$ pip3 install -r requirements.txt
```

2. Create `config.cfg` (see below).

## Actions
`Actions` are executed by the script to get and send information to Slack.
`Actions` should be placed in the `action/` directory and follow the naming
scheme:
- filename: `my_cool_action.py`
- class name: `MyCoolACtion`

`Actions` will be loaded automatically using
[reflection](https://en.wikipedia.org/wiki/Reflection_(computer_programming))
if the naming scheme above is followed.

The `Actions` to run are specified in `config.cfg` and are listed in the order
they should be ran in. For example:
```
[ACTION]
run = good_morning, weather, current_events
```

will execute `GoodMorningAction`, `WeatherAction`, and `CurrentEventsAction` in
that order.

## Current Actions
### `GoodMorningAction`
Sends the current date and the quote of the day. Quotes of the day are scraped
from [https://quotes.rest/](https://quotes.rest/).

### `WeatherAction`
Sends the current weather information. Weather information is scraped from
[https://openweathermap.org/api](https://openweathermap.org/api) which requires
an API key which can be be gotten from [here](https://openweathermap.org/appid).

### `CurrentEventsAction`
Gets several brief descriptions of current events from the previous day from
Wikipedia's
[Current Events Portal](https://en.wikipedia.org/wiki/Portal:Current_events).

### `CalvinAndHobbesAction`
Gets the daily Calvin and Hobbes comic strip from
[https://www.gocomics.com/calvinandhobbes](https://www.gocomics.com/calvinandhobbes).

## Configuration file
`good_morning_slack` uses the configuration file `config.cfg` which should be
located in the same directory as `good_morning_slack.py`. An example
configuration file is provided in `example.cfg`.
