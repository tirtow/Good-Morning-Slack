from configparser import ConfigParser
import importlib
from os import listdir
from os.path import join, dirname, realpath
from slacker import Slacker
import traceback


def main():
    """
    Load and execute each Action
    """

    # Load the config
    config = _load_config()
    channel = "#" + config["SLACK"]["channel"]
    error_channel = "#" + config["SLACK"]["error_channel"]

    # Get the slacker
    slacker = Slacker(config["SLACK"]["token"])

    # Get the actions
    action_names = map(lambda s: s.strip(), config["ACTION"]["run"].split(","))
    actions = _load_actions(slacker, error_channel)

    # Execute each action
    attachments = []
    for key in action_names:
        try:
            action = actions[key](config)
            print("Executing %s" % str(action))
            action.execute(attachments)
        except Exception as e:
            # Something went wrong, sent error message
            _send_error(slacker, error_channel, e, traceback.format_exc())

    # Send the attachments
    _send_attachments(slacker, channel, attachments)


def _send_attachments(slacker, channel, attachments, num_at_time=20):
    """
    Sends attachments to slack num_at_time at a time.

    slacker         -- the slacker
    channel         -- the channel to send to
    attachments     -- the list of attachments
    num_at_time     -- the number of attachments to send at a time. (default 20)
    """

    z = zip(range(0, len(attachments), num_at_time),
            range(num_at_time, len(attachments), num_at_time))
    if len(attachments) <= 20:
        z = [(0, len(attachments))]

    # Send the attachments
    for b, e in z:
        slacker.chat.post_message(channel, attachments=attachments[b:e])


def _load_actions(slacker, channel):
    """
    Gets a dict mapping the name of an action to its class type

    slacker     -- the slacker
    channel     -- the channel to send error messages to

    Returns the dict
    """

    result = {}

    # Loop through files in action/
    path = join(dirname(realpath(__file__)), "action")
    for f in listdir(path):
        if f.endswith("_action.py"):
            try:
                # Remove the ".py"
                name = f[:-3]

                # Load the module
                module = importlib.import_module("action." + name)

                # Get the dict key
                key = name[:-len("_action")]

                # Load the class
                class_name = "".join(map(lambda s: s.title(), name.split("_")))
                result[key] = getattr(module, class_name)
            except Exception as e:
                # Failed to load a file, send error message
                _send_error(slacker, channel, e, traceback.format_exc())

    return result


def _send_error(slacker, channel, e, trace):
    """
    Sends error message to the given channel

    slacker     -- the slacker
    channel     -- the channel to send to
    e           -- the exception raised
    trace       -- the traceback
    """

    slacker.chat.post_message(channel, str(e))
    slacker.chat.post_message(channel, "```%s```" % trace)


def _load_config():
    """
    Read in the config file
    """

    config = ConfigParser()
    config.read(join(dirname(realpath(__file__)), "config.cfg"))
    return config


# Run the script
if __name__ == "__main__":
    main()
