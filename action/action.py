import re


class Action:
    """
    The interface Actions must implement to be properly loaded and ran.
    """

    def __init__(self, config):
        """
        Store the configuration
        """

        self.config = config

    def execute(self, attachments):
        """
        Executes this Action.

        The Action should append any attachments to be sent to attachments.

        attachments     -- the list of attachments to append to.

        Must be implemented by inheriting classes.
        """

        raise NotImplementedError("%s must implement execute()" %
                                  str(type(self)))

    def __str__(self):
        """
        Return the name of this class
        """

        pattern = "'\S+'"
        return re.search(pattern, str(type(self))).group(0)[1:-1]
