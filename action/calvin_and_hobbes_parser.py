from html.parser import HTMLParser


class CalvinAndHobbesParser(HTMLParser):
    """
    Parses out the daily Calvin and Hobbes image.
    """

    def feed(self, text):
        """
        Override feed() to initialize some variables and return the info parsed.
        """

        self.found_image = False
        self.info = {}

        # Call HTMLParser's feed()
        super().feed(text)

        return self.info

    def handle_starttag(self, tag, attr):
        """
        Checks if found the image and parses it if so.
        """

        if tag == "picture" and ("class", "item-comic-image") in attr:
            # Found the <picture> tag containing the image
            self.found_image = True

        if self.found_image and tag == "img":
            # Found the image
            attr = self._attr_to_dict(attr)
            self.info["title"] = attr["alt"].strip()
            self.info["image_url"] = attr["src"]
            self.found_image = False

    def _attr_to_dict(self, attr):
        """
        Converts attr from tuples to a dict
        """

        return {k: v for k, v in attr}
