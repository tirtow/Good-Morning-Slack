from html.parser import HTMLParser

from action.current_events_section import CurrentEventsSection


class CurrentEventsParser(HTMLParser):
    """
    Handles parsing a wikipedia current events page to scrape current events.
    """

    def feed(self, text):
        """
        Override HTMLParser's feed to do some setup and return a list of
        Sections
        """

        self.in_description = False     # Whether or not in description
        self.in_section = False         # Whether or not in section
        self.open_uls = 0               # Number of <ul> tags in
        self.in_post = False            # Whether or not in post in section
        self.in_attachment = False      # Whether or not handling external link

        # Used to build the post information
        self.text = ""
        self.link = {"text", "url"}

        # List of Sections
        self.sections = []

        # Call HTMLParser's feed()
        super().feed(text)

        return self.sections

    def handle_starttag(self, tag, attr):
        """
        Handles start tags to check which part of the file the parser is in.
        """

        # Check if entered description
        self._set_entered_description(tag, attr)

        # Check if entered a section
        self._set_entered_section(tag, attr)

        # Check if entered a <ul>
        self._set_entered_ul(tag)

        # Check if entered a post
        self._handle_entered_post(tag)

        # Check if found external link
        self._handle_entered_post_link(tag, attr)

    def handle_endtag(self, tag):
        """
        Handles end tags to check which part of the file the parser is in.
        """

        # Check if left a section
        self._set_left_section(tag)

        # Check if left a post
        self._set_left_post(tag)

    def handle_data(self, data):
        """
        Handles adding data to the appropriate location
        """

        # Create section if data is the name
        self._handle_create_section(data.strip())

        # Add text to post
        self._add_post_text(data)

        # Add text to external link
        self._add_link_text(data)

    def _set_entered_description(self, tag, attr):
        """
        Sets whether or not just entered the description div

        tag     -- the tag to check
        attr    -- the attributes
        """

        if not self._in_description() and tag == "div" and \
                ("class", "description") in attr:
            self.in_description = True

    def _set_entered_section(self, tag, attr):
        """
        Sets whether or not just entered a section

        tag     -- the tag
        attr    -- the attributes
        """

        if self._in_description() and tag == "div" and \
                ("role", "heading") in attr:
            self.in_section = True

    def _set_entered_ul(self, tag):
        """
        Increments self.open_uls if in section and tag is "ul"

        tag     -- the tag to check
        """

        if self._in_section() and tag == "ul":
            self.open_uls += 1

    def _handle_entered_post(self, tag):
        """
        If just entered a post, do the setup to parse that post.

        tag     -- the tag to check
        """

        if self._in_section() and tag == "li":
            self.in_post = True
            self.text = ""
            self.link = {"text": "", "url": ""}

    def _handle_entered_post_link(self, tag, attr):
        """
        If just entered the external link for a post, do the setup and parse it.

        tag     -- the tag to check
        attr    -- the attributes
        """

        if self._in_post() and tag == "a":
            if ("class", "external text") in attr:
                # Found the external link
                self.in_attachment = True

                # Find the URL
                for field, text in attr:
                    if field == "href":
                        self.link["url"] = text
                        break

    def _set_left_section(self, tag):
        """
        If left a <ul> tag decrement self.open_uls and check if left section.

        tag     -- the tag to check
        """

        if self._in_section() and tag == "ul":
            self.open_uls -= 1

            if self.open_uls == 0:
                # Found end of a section
                self.in_section = False
                self.in_post = False

    def _set_left_post(self, tag):
        """
        If left a post, clean up and add the post to self.sections

        tag     -- the tag to check
        """

        if self._in_post() and tag == "li":
            self.in_post = False
            self.in_attachment = False
            section = self.sections[len(self.sections) - 1]
            section.add_post(self.text, self.link)

    def _handle_create_section(self, name):
        """
        Check if have the name of a section and create the section

        name    -- the name of the section
        """

        if self._in_section() and not self._in_post() and len(name) > 0:
            self.sections.append(CurrentEventsSection(name))

    def _add_post_text(self, text):
        """
        Adds to the text for a post

        text    -- the text to add
        """

        if self._in_post() and not self._in_attachment():
            self.text += text

    def _add_link_text(self, text):
        """
        Adds to the text for an external link

        text    -- the text to add
        """

        if self._in_attachment():
            self.link["text"] += text

    def _in_description(self):
        """
        Get whether or not in the description
        """

        return self.in_description

    def _in_section(self):
        """
        Get whether or not in a section
        """

        return self._in_description() and self.in_section

    def _in_post(self):
        """
        Get whether or not in a post
        """

        return self._in_section() and self.in_post

    def _in_attachment(self):
        """
        Get whether or not in external link
        """

        return self._in_post() and self.in_attachment
