class CurrentEventsSection:
    """
    Handles creating a current events section.
    """

    def __init__(self, name):
        """
        Initialize this section given its name
        """

        self.name = name
        self.posts = []

    def add_post(self, text, link):
        """
        Adds a post to this section.

        text    -- the text of the post
        link    -- a dict with the keys "text" and "url"
        """

        self.posts.append({
            "text": text,
            "link": link,
        })

    def attachment(self):
        """
        Returns this Section as a list of attachements
        """

        # Create the result dict
        result = {
            "title": self.name,
            "color": "#F27171",
            "fallback": self.name,
            "fields": [],
            "actions": []
        }

        # Add each post as its own field
        for post in self.posts:
            # Add the field
            result["fields"].append({
                "title": post["link"]["text"],
                "value": post["text"]
            })

            # Add the button
            result["actions"].append({
                "type": "button",
                "text": post["link"]["text"],
                "url": post["link"]["url"]
            })

        return [result]
