from datetime import datetime, timedelta
import requests

from action.action import Action
from action.calvin_and_hobbes_parser import CalvinAndHobbesParser


class CalvinAndHobbesAction(Action):
    """
    Gets a daily Calvin and Hobbes image.
    """

    FAILED_TEXT = "Sorry but there was no Calvin and Hobbes for that date."

    def __init__(self, config):
        """
        Store the configuration
        """

        # Call Action's constructor
        super().__init__(config)

    def execute(self, attachments):
        """
        Get the daily comic and append as attachment.

        attachments     -- the list to append to
        """

        # Make the request
        days_back = 0

        for i in range(2):
            r = self._make_request(days_back)
            if r:
                if CalvinAndHobbesAction.FAILED_TEXT in r.text:
                    # Day ahead of the site, go back a day
                    days_back = 1
                else:
                    # Parse the response
                    info = self._parse_info(r)
                    attachments += self._build_attachments(info)
                    break

    def _make_request(self, days_back=0):
        """
        Makes the request to gocomics
        """

        day = datetime.utcnow() - timedelta(days=days_back)

        url = "https://www.gocomics.com/calvinandhobbes/" + \
            day.strftime("%Y/%m/%d")
        return requests.get(url)

    def _parse_info(self, response):
        """
        Parses the HTML from the response to build the info dict
        """

        parser = CalvinAndHobbesParser()
        info = parser.feed(response.text)
        info["url"] = response.url

        return info

    def _build_attachments(self, info):
        """
        Builds the attachment to send.

        info    -- the dict with information

        Returns a list of dicts of attachments
        """

        return [{
            "title": info["title"],
            "color": "#FFAA00",
            "fallback": info["title"],
            "footer": info["url"],
            "image_url": info["image_url"]
        }]
