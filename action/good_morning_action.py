from datetime import datetime
import requests
import math

from action.action import Action


class GoodMorningAction(Action):
    """
    The first Action to run that gets the date and the quote of the day
    """

    def __init__(self, config):
        """
        Store the configuration
        """

        # Call Action's constructor
        super().__init__(config)

    def execute(self, attachments):
        """
        Send the good morning message
        """

        attachments += self._build_attachments()

    def _build_attachments(self):
        """
        Builds the attachments to send to slack.

        Returns a list of dicts of attachments
        """

        now = datetime.utcnow()
        return [{
            "title": now.strftime("Today is %A, %B %d, %Y"),
            "color": "#FFEE00",
            "fallback": now.strftime("Today is %A, %B %d, %Y"),
            "footer": "https://quotes.rest/",
            "fields": [
                {
                    "title": "Day of Year",
                    "value": "It is the *%s* day of the year." %
                             self._ordinal(int(now.strftime("%j"))),
                    "short": "true"
                },
                {
                    "title": "Week of Year",
                    "value": "It is the *%s* week of the year." %
                             self._ordinal(int(now.strftime("%U"))),
                    "short": "true"
                },
                {
                    "title": "Quote of the Day",
                    "value": self._get_qod(),
                }
            ]
        }]

    def _get_qod(self):
        """
        Request the quote of the day and return the formatted message string.
        """

        headers = {"Accept": "application/json"}
        r = requests.get("https://quotes.rest/qod", headers=headers)
        if r:
            json = r.json()["contents"]["quotes"][0]
            return "%s\n- _%s_" % (json["quote"], json["author"])
        else:
            return "API call to QOD failed."

    def _ordinal(self, n):
        """
        Convert n to ordinal taken from https://stackoverflow.com/a/20007730
        """

        return "%d%s" % (n, "tsnrhtdd"[(math.floor(n/10) % 10 != 1) *
                                       (n % 10 < 4) * n % 10::4])
