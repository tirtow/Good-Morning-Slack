from datetime import date, timedelta
import requests

from action.action import Action
from action.current_events_parser import CurrentEventsParser


class CurrentEventsAction(Action):
    """
    Handles requesting current events from wikipedia
    """

    def __init__(self, config):
        """
        Store configuration variables
        """

        # Call Action's constructor
        super().__init__(config)

    def execute(self, attachments):
        """
        Requests the current events and append then to attachments.

        attachments     -- the list to append to
        """

        # Get yesterday's date
        yest = date.today() - timedelta(days=1)
        yest = yest.strftime("%Y_%B_%-d")

        # Make the request
        url = "https://en.wikipedia.org/wiki/Portal:Current_events/%s" % yest
        r = requests.get(url)

        if r:
            for section in self._build_sections(r.text):
                attachments += section.attachment()

    def _build_sections(self, text):
        """
        Parse the sections from text

        text    -- the text to parse

        Returns a list of Sections
        """

        parser = CurrentEventsParser()
        return parser.feed(text)
