import requests

from action.action import Action


class WeatherAction(Action):
    """
    Gets current weather information
    """

    # The URL to request to
    URL = "http://api.openweathermap.org/data/2.5/weather"

    def __init__(self, config):
        """
        Get config values needed to run
        """

        # Call Action's constructor
        super().__init__(config)

        # Store config variables
        self.apikey = config["WEATHER"]["apikey"]
        self.channel = "#" + config["SLACK"]["channel"]
        self.zipcode = config["WEATHER"]["zipcode"]

    def execute(self, attachments):
        """
        Request the weather information and send it.

        attachments     -- the list to append attachments to
        """

        # Retry request up to 5 times if fails
        for i in range(5):
            # Make the request
            params = {
                "APPID": self.apikey,
                "zip": self.zipcode,
                "units": "imperial"
            }
            r = requests.get(WeatherAction.URL, params=params)

            # Send the message
            if r:
                attachments += self._build_attachments(r.json())
                break

    def _build_attachments(self, json):
        """
        Builds the weather message attachments to send.

        json    -- the response dict

        Returns a list of dicts of attachments
        """

        return [{
            "title": self._title(json),
            "color": "#00BF0F",
            "fallback": self._title(json),
            "footer": "https://openweathermap.org/api",
            "fields": [
                {
                    "title": "Temperature",
                    "value": self._temperature_string(json),
                    "short": "true"
                },
                {
                    "title": "Conditions",
                    "value": self._conditions_string(json),
                    "short": "true"
                },
                {
                    "title": "Sunrise",
                    "value": self._format_date(json["sys"]["sunrise"]),
                    "short": "true"
                },
                {
                    "title": "Sunset",
                    "value": self._format_date(json["sys"]["sunset"]),
                    "short": "true"
                }
            ]
        }]

    def _title(self, json):
        """
        Builds the title for the attachment.

        json    -- the dict with info

        Returns the string
        """

        return "Weather for %s, %s" % (json["name"], json["sys"]["country"])

    def _temperature_string(self, json):
        """
        Builds the value string for temperatures.

        json    -- the dict with info

        Returns the string
        """

        msg = "- Current: *%.2f°*\n" % json["main"]["temp"]
        msg += "- Low: *%.2f°*\n" % json["main"]["temp_min"]
        msg += "- High: *%.2f°*\n" % json["main"]["temp_max"]

        return msg

    def _conditions_string(self, json):
        """
        Builds the value string for temperatures.

        json    -- the dict with info

        Returns the string
        """

        msg = "- Description: *%s*\n" % json["weather"][0]["description"]
        msg += "- Temperature: *%.2f°*\n" % json["main"]["temp"]
        msg += "- Humidity: *%d%%*\n" % json["main"]["humidity"]
        msg += "- Windspeed: *%.2f mph*\n" % json["wind"]["speed"]

        return msg

    def _format_date(self, timestamp, fallback="Failed"):
        """
        Formats a timestamp for slack

        timestamp       -- the timestamp to format
        fallback        -- the fallback string. (default "Failed")

        Returns the string to send to slack
        """

        return "<!date^%s^{time_secs}|%s>\n" % (timestamp, fallback)
